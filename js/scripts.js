// JavaScript file for English in Munich website

// Google Maps API
var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	center: {lat: 48.1270, lng: 11.5784},
	zoom: 11
	});
};

// jQuery Functions
$(document).ready(function(){
	// jQuery load top banners
	$(".header-content h1").delay(600).fadeIn(3000);
	$(".header-content h2").delay(600).fadeIn(5000);

	// Smooth Scrolling
	var $root = $('html, body');
	$('.navbar a').click(function() {
		var href = $.attr(this, 'href');
		$root.animate({
			scrollTop: $(href).offset().top
		}, 500, function () {
		window.location.hash = href;
		});
		return false;
	});

	// Contact Form Submission
	var $contactForm = $('#contact-form');
	$contactForm.submit(function(e) {
		e.preventDefault();

		var formName = $("#form-name").val();
		var formCompany = $("#form-company").val();
		var formEmail = $("#form-email").val();
		var formTel = $("#form-telephone").val();
		var formComment = $("#message-box").val();

		$.ajax({
			url: '//formspree.io/chrwimct@gmail.com',
			method: 'POST',
			data: $(this).serialize(),
			dataType: 'json',
			beforeSend: function() {
				$("#visible-status").html('<div class="alert alert-info alert-loading">Sending message.</div>');
			},
			success: function(data) {
				$("#visible-name").html(formName);
				$("#visible-company").html(formCompany);
				$("#visible-email").html(formEmail);
				$("#visible-telephone").html(formTel);
				$("#visible-comment").html(formComment);

				$("#form-name").hide();
				$("#form-company").hide();
				$("#form-email").hide();
				$("#form-telephone").hide();
				$("#message-box").hide();
				$("#form-button").hide();
				$contactForm.find('.alert-loading').hide();
				$("#visible-status").html('<div class="alert alert-success">Message sent.</div>');
				$(".alert").delay(4000).fadeOut(800);
			},
			error: function(err) {
				$contactForm.find('.alert-loading').hide();
				$("#visible-status").html('<div class="alert alert-danger">Sorry, there was an error.</div>');
				$(".alert").delay(4000).fadeOut(800);
			}
		});
	});

	// Google Maps API Resize in Modal
	$('.google-map').on('shown.bs.modal', function () {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center);
	});

});
