# README

This is the code for a personal static website.

## General Information

Created: Spring 2016

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/english-in-munich)

Website: [Link](https://english-in-munich.netlify.com/)

Please feel free to send me any comments or feedback.

Last updated: 2024-10-23
